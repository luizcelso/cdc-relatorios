

#Relatórios CDC-Brasil
**Disseminar o conhecimento. Combater a desinformação.**

O Relatórios CDC-Brasil é uma ação do projeto [Ciência de Dados por uma Causa](https://dadosecausas.org/) focada em desenvolver e compartilhar livremente análises sobre a realidade brasileira. Nossos valores incluem didatismo na comunicação,  imparcialidade/independência nas análises e liberdade de acesso a dados e código.

## Iniciativas

A principal iniciativa em desenvolvimento atualmente está focada em ferramentas e análises relacionados à pandemia COVID-19;

Os esforços estão sendo publicados no site [Como está o meu país - COVID-19](https://coronavirus.dadosecausas.org/).

Análises e códigos estão no diretório `coronaviris` neste repositório.