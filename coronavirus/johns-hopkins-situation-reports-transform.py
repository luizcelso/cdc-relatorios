import csv
from datetime import datetime
import re
import pandas as pd # $ pip3 install xlrd

f = csv.writer(open('data/john-hopkins-processed.csv', 'w'))
f.writerow(['Date', 'Country', 'Desc', 'FullText'])

# countries_csv = csv.reader(open('all_countries.csv'), delimiter=',')
# countries = []
# for row in countries_csv:
#     countries.append(row[0])

countries_xlsx = pd.read_excel('data/country_names.xlsx')
countries_xlsx.drop(['ISO_3166_alpha_3', 'names_temperature', 'World Bank', 'United Nations Population Division','WHO',	'WHO_HIV'], axis=1, inplace=True)
countries_xlsx.drop([0], inplace=True)
countries_xlsx.dropna(subset=['OWID_names'], inplace=True)
print(countries_xlsx.head())

with open('data/john-hopkins-collect.csv', 'r') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    next(csv_reader)

    for row in csv_reader:
    	collectDate = row[0].replace(',', '').replace(' am', '').replace(' pm', '').replace(' correction', '')
    	formatDate = datetime.strptime(collectDate, '%d %B %Y').date()

    	new = ". " + row[1]

    	# for country in countries:
    	# 	regex = re.compile("([^.]*"+country+"[^.]*.)")
    	# 	result = regex.search(new)
    	# 	if result:
    	# 		match = result.group(1)
    	# 		match.strip()
    	# 		f.writerow([formatDate, country, match, row[1]])
    	# 		print(new[:10])

    	for index, row in countries_xlsx.iterrows():
    		current_country = row['OWID_names']
    		regex = re.compile("([^.]*"+current_country+"[^.]*.)")
    		result = regex.search(new)
    		if result:
    			match = result.group(1)
    			print(current_country)
    			f.writerow([formatDate, current_country, str(match).strip(), new[2:]])
    			print(new[:10])

    		others = str(row['other']).split(', ')
    		for word in others:
    			if word == 'nan': break
    			if word:
	    			regex = re.compile("([^.]*"+word+"[^.]*.)")
		    		result2 = regex.search(new)
		    		if result2:
		    			match = result2.group(1)
		    			f.writerow([formatDate, current_country, str(match).strip(), new[2:]])
		    			print(new[:10])

    	print(formatDate)