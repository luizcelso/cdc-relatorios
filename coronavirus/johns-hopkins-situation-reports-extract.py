# coding: utf-8

import requests
import urllib3.request
import time
from bs4 import BeautifulSoup
import re
import csv
import sys

url = 'http://www.centerforhealthsecurity.org/resources/COVID-19/COVID-19-SituationReports.html'
links = []
dates = []
Country = []
Descr = []
FullText = []

response = requests.get(url)

soup = BeautifulSoup(response.text, 'html.parser')

f = csv.writer(open('data/john-hopkins-collect.csv', 'w'))
f.writerow(['Date', 'News'])

for link in soup.findAll('a', attrs={'href': re.compile("^https://myemail")}):
	links.append(link)

for i in links:
	day = requests.get(i.get("href"))
	soup_day = BeautifulSoup(day.text, 'html.parser')

	date = ''
	date = re.sub(r"(<[^>]*>)", r' ', str(i)).replace("\n", '').strip()
	date += ', 2020'


	x = soup_day.select('td.editor-text > div.text-container > div > div:nth-of-type(1)')
	j = 1
	beginnings = ["EPI", "GLOBAL SITUATION REPORT", "REGION", "CORRECTION", "UPDATES"]
	while not any(beginning in str(x) for beginning in beginnings):
		j += 1
		x = soup_day.select('td.editor-text > div.text-container > div > div:nth-of-type('+str(j)+')')

	while x:
		x = re.sub(r"(<[^>]*>)", r' ', str(x[0])).replace("\n", '').strip()

		if x:
			f.writerow([date.encode(sys.stdout.encoding, errors='replace'), x.encode(sys.stdout.encoding, errors='replace')])
		
		j += 1
		x = soup_day.select('td.editor-text > div.text-container > div > div:nth-of-type('+str(j)+')')

	# resultSet = soup_day.findAll('div', class_ = 'text-container')
	# news = BeautifulSoup(resultSet[2].text, 'html.parser')
	# print news.findAll('div')

	print (date)