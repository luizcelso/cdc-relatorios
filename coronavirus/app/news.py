from wordpress_xmlrpc import Client
from wordpress_xmlrpc.methods import posts
from wordpress_xmlrpc.methods import users
import os

client = Client('https://dadosecausas.org/xmlrpc.php', os.getenv("WORDPRESS_USER"),  os.getenv("WORDPRESS_PASS"))
posts_blog = client.call(posts.GetPosts({'post_status': 'publish'}))
authors = client.call(users.GetAuthors())

def get_posts():
    blog_posts = [];
    for post in posts_blog:
        for term in post.terms:
            if (term.slug == 'covid19'):
                blog_posts.append(post)
    return blog_posts

def get_post_by_id(slug):
    for post in posts_blog:
        # get author name
        for author in authors:
            if post.user == author.id:
                authorname = author.display_name
        # test if covif19 slug
        if post.slug == slug:
            for term in post.terms:
                if (term.slug == 'covid19'):
                    return post, authorname
