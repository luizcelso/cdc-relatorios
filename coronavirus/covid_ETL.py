#!/usr/bin/env python
# coding: utf-8

# # COVID-19 Data Processing
# 
# 
# COVID-19 cases data from [Our World in Data](www.ourworldindata.org)
# 

# In[50]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import seaborn as sns; sns.set()
from datetime import datetime
import calendar
import os

pd.set_option('display.max_rows', 6000)
pd.set_option('display.max_columns', 100)
pd.set_option('display.width', 1000)


# ## Gather source data
# 
# Total cases and total deths by contry data from ourworldindata.org
# 

# In[51]:


data_dir = 'data/'

# df_orig = pd.read_csv(data_dir + 'total-cases-covid-19-who.csv')

url_cases = 'https://covid.ourworldindata.org/data/ecdc/total_cases.csv'

# sort values and fill missing values with last
df_source_cases = pd.read_csv(url_cases).sort_values('date').ffill()
    
# df_orig.columns = ['Country', 'Code', 'Day', 'Total']


# In[52]:


df_orig = df_source_cases

df_orig['Day'] = df_orig.index

df_orig = df_orig.melt(['Day', 'date']).rename(columns={'date':'Date', 'value':'Total', 'variable':'Country'})

df_orig = df_orig[['Country', 'Date', 'Total', 'Day']]

df_orig.head()


# In[53]:


# df_deaths = pd.read_csv(data_dir + 'total-deaths-covid-19-who.csv')

# df_deaths.columns = ['Country', 'Code', 'Day', 'TotalDeaths']

url_deaths = 'https://covid.ourworldindata.org/data/ecdc/total_deaths.csv'

# sort values and fill missing values with last
df_source_deaths = pd.read_csv(url_deaths).sort_values('date').ffill()

df_source_deaths.head()


# In[54]:


df_deaths = df_source_deaths

df_deaths['Day'] = df_deaths.index

df_deaths = df_deaths.melt(['Day', 'date']).rename(columns={'date':'Date', 'value':'TotalDeaths', 'variable':'Country'})

df_deaths = df_deaths[['Country', 'Date', 'TotalDeaths', 'Day']]

df_deaths

df_deaths.head()


# In[55]:


df_orig = pd.merge(df_orig, df_deaths, how = 'left')

df_orig.head()


# ## Data Cleaning
# 
# 

# In[56]:


df_orig = df_orig.dropna()

df_orig = df_orig[~df_orig['Country'].isin(['World', 'International'])]

df_orig = df_orig.sort_values(['Country', 'Date'])

df_orig.head()


# In[57]:


# generates DataFrame with all countries in the dataset

df_all_countries = pd.DataFrame({'Country': df_orig['Country'].unique()})

df_all_countries.head()


# In[58]:


# generates DataFrame with all countries with more than 10 deaths

df_all_countries_compare = pd.DataFrame({'Country': df_orig.query('TotalDeaths > 10')['Country'].unique()})

df_all_countries_compare.head()


# ## Adding daily data

# In[59]:


df_orig['TotalPrev'] = df_orig.groupby(['Country']).shift(1)['Total'].fillna(0)
df_orig['TotalDeathsPrev'] = df_orig.groupby(['Country']).shift(1)['TotalDeaths'].fillna(0)

df_orig['DailyDeaths'] = df_orig['TotalDeaths'] - df_orig['TotalDeathsPrev']
df_orig['DailyCases'] = df_orig['Total'] - df_orig['TotalPrev']

df_orig.drop(['TotalPrev', 'TotalDeathsPrev'], axis=1, inplace=True)
              
df_orig.head(1000)


# ## Adding per 100k stats
# 
# 

# In[60]:


df_pop = pd.read_csv(data_dir + 'socio_stats_countries.csv')

df_pop = df_pop[df_pop['variable'] == 'Population'].drop(['score', 'variable'], axis=1).rename(columns={'Name': 'Country', 'value': 'Population'})

df_pop.head()


# In[61]:


df_orig = pd.merge(df_orig, df_pop)

df_orig.head()


# In[62]:


df_orig['TotalPer100k'] = df_orig['Total'] / df_orig['Population'] * 100000
df_orig['TotalDeathsPer100k'] = df_orig['TotalDeaths'] / df_orig['Population'] * 100000
df_orig['DailyDeathsPer100k'] = df_orig['DailyDeaths'] / df_orig['Population'] * 100000
df_orig['DailyCasesPer100k'] = df_orig['DailyCases'] / df_orig['Population'] * 100000
df_orig = df_orig.drop('Population', axis = 1)

df_orig.head()


# ## Normalizing days to start after 50 cases
# 
# (not used currently)

# In[63]:


df_orig_start_day = df_orig[df_orig['Total'] >= 50]

df_orig_start_day.head()


# Start counting the days after first day with 50 total cases.

# In[64]:


df_orig_start_day = df_orig[df_orig['Total'] >= 50]

df_orig_start_day.head()


# In[65]:


df_orig_start_day['DayNorm'] =  df_orig_start_day[['Day']] - df_orig_start_day.groupby(['Country'])[['Day']].transform('min') 

df_orig_start_day.sample(10)

df_orig_start_day = df_orig_start_day[['Country', 'Day', 'DayNorm']]

df_orig_start_day.head()


# In[66]:


df_orig_norm = pd.merge(df_orig, df_orig_start_day, how='left').sort_values(['Country', 'Day'])

df_orig_norm.head()


# ## Calculate daily growth rates

# In[67]:


df_growth = df_orig_norm.copy().sort_values(['Country', 'Date'])

df_growth.head()


# In[68]:


df_growth_grouped = df_growth.groupby(['Country', 'Day']).max()[['Total', 'TotalDeaths']].sort_index()

df_growth_grouped.head(10)

df_growth_grouped.head()


# In[69]:


df_growth_grouped['TotalPrev'] = df_growth_grouped.groupby(['Country']).shift(1)['Total']

df_growth_grouped['GrowthRate'] = df_growth_grouped['Total']/df_growth_grouped['TotalPrev']

df_growth_grouped['TotalPrevDeaths'] = df_growth_grouped.groupby(['Country']).shift(1)['TotalDeaths']

df_growth_grouped['GrowthRateDeaths'] = df_growth_grouped['TotalDeaths']/df_growth_grouped['TotalPrevDeaths']

df_growth_grouped = df_growth_grouped.replace([np.inf, -np.inf], np.nan)

df_growth_grouped.head()


# In[70]:


df_growth_rate = df_growth_grouped[['GrowthRate', 'GrowthRateDeaths']].dropna(subset=['GrowthRate'])

df_growth_rate.head()


# In[71]:


df_complete = pd.merge(df_orig_norm, df_growth_rate.reset_index(), how='left')

df_complete.head()


# In[72]:



df_complete['DaysToDouble'] = df_complete['GrowthRate'].apply(lambda x: math.log(2, x) if x > 1 else x)

df_complete['DaysToDoubleDeaths'] = df_complete['GrowthRateDeaths'].apply(lambda x: math.log(2, x) if x > 1 else x)

df_complete.head()


# ## Calculate weekly growth
# 
# 

# In[73]:


df_complete.head()


# In[74]:


df_complete[['WeeklyGrowth', 'WeeklyGrowthDeaths']] = (df_complete.groupby(['Country'])[['Total', 'TotalDeaths']].pct_change(periods=7) * 100).replace([np.inf, -np.inf], np.nan)

df_complete.head()


# ## Generate last day stats by country
# 

# In[75]:


df_complete.set_index('Country').loc['Brazil']


# In[76]:


# last two days
dates = df_complete['Date'].unique()[-2:]

columns_stats = ['Total', 'DailyCases', 'TotalPer100k', 'GrowthRate', 'DaysToDouble', 'WeeklyGrowth', 'TotalDeaths', 'DailyDeaths', 'TotalDeathsPer100k', 'GrowthRateDeaths', 'DaysToDoubleDeaths', 'WeeklyGrowthDeaths']

df_countries_before = df_complete[df_complete['Date'] == dates[0]].set_index('Country')[columns_stats]

df_countries_now = df_complete[df_complete['Date'] == dates[1]].set_index('Country')[columns_stats]

df_countries_stats_change = ((df_countries_now/df_countries_before) - 1) * 100

df_countries_stats_change = df_countries_stats_change.reset_index().melt('Country').rename(columns={'value':'change'})

df_countries_stats_complete = pd.merge(df_countries_now.reset_index().melt('Country'), df_countries_stats_change)

df_countries_stats_complete.head()


# ## Situation reports
# 
# 

# In[77]:


# df_events_sr = pd.read_csv(data_dir + 'situation_reports_countries.csv')

df_events_sr = pd.read_csv(data_dir + 'john-hopkins-processed.csv')

df_events_sr['Source'] = 'Johns Hopkins Situation Reports'

df_events_sr['Country'] = df_events_sr['Country'].str.replace('US', 'United States')

df_events_sr.head()


# In[78]:


df_events_sr_quar = df_events_sr[df_events_sr['FullText'].str.contains('quarantine|lockdown|stay at home|stay-at-home|curfew', regex=True, case=False)]

df_events_sr_quar = df_events_sr_quar.groupby(['FullText', 'Date'])['Country'].apply(lambda x: ",".join(x)).reset_index()

df_events_sr_quar.head()


# In[79]:


df_keywords = pd.read_csv(data_dir + 'keywords_JH_reports.csv')

df_keywords_grouped = df_keywords.groupby(['highlight'])['keyword'].apply(lambda x: "|".join(x))

for h, k in df_keywords_grouped.iteritems():
    df_events_sr.loc[df_events_sr['FullText'].str.contains(k), 'Highlight'] = h

df_events_sr['Highlight'] = df_events_sr['Highlight'].fillna(0)

df_events_sr.head()


# ## Response variables

# In[80]:


df_resp = pd.read_csv('https://oxcgrtportal.azurewebsites.net/api/CSVDownload', parse_dates = ['Date'])

df_resp.head()


# In[81]:


# normalize names

df_countries_names = pd.read_excel(data_dir + 'country_names.xlsx').drop(0, axis=0)

df_countries_names_conv = df_countries_names[['ISO_3166_alpha_3', 'OWID_names']].dropna()

df_countries_names_conv.columns = ['CountryCode', 'Country']

df_countries_names_conv.head()


# In[82]:


df_resp = df_countries_names_conv.merge(df_resp).drop(['CountryName', 'CountryCode'], axis=1)

df_resp


# In[83]:


var_cols = [c for c in df_resp.columns if '_' in c]

notes_cols = [c for c in df_resp.columns if c.endswith('Notes')]

var_names = pd.DataFrame([(n.split('_')[0], n.split('_')[1]) for n in var_cols if not n.endswith(('Flag', 'Notes'))], columns=['code', 'label']).set_index('code')

# var_names['type'] = 'Containment'
# var_names.loc[var_names.index.str.startswith('H'),'type'] = 'Health'
# var_names.loc[var_names.index.str.startswith('E'),'type'] = 'Economic'
var_names.loc['M1', 'label'] = 'Other'
# var_names.loc['M1', 'type'] = 'Other'

var_names.sample(5)


# In[84]:


df_resp.columns


# In[85]:


int_cols = ['C1_School closing',
 'C1_Flag',
 'C2_Workplace closing',
 'C2_Flag',
 'C3_Cancel public events',
 'C3_Flag',
 'C4_Restrictions on gatherings',
 'C4_Flag',
 'C5_Close public transport',
 'C5_Flag',
 'C6_Stay at home requirements',
 'C6_Flag',
 'C7_Restrictions on internal movement',
 'C7_Flag',
 'C8_International travel controls',
 'E1_Income support',
 'E1_Flag',
 'E2_Debt/contract relief',
 'H1_Public information campaigns',
 'H1_Flag',
 'H2_Testing policy',
 'H3_Contact tracing',
 'ConfirmedCases',
 'ConfirmedDeaths']

resp_cols = [
     'StringencyIndex',
 'StringencyIndexForDisplay',
 'LegacyStringencyIndex',
 'LegacyStringencyIndexForDisplay']

int_cols


# In[86]:


df_resp[int_cols] = df_resp[int_cols].astype(pd.Int64Dtype())

df_resp.head()


# ### Containment response dataframe

# In[87]:


# containment_cols = list(df_resp.columns[df_resp.dtypes.eq('float64')][:15]) + ['ConfirmedDeaths']

df_resp_containment = df_resp[['Country', 'Date', 'StringencyIndexForDisplay', 'ConfirmedDeaths']]#.rename(columns={'Date':'Start', 'Name':'Task'})
df_resp_containment['StringencyIndexForDisplay'] = df_resp_containment['StringencyIndexForDisplay'].fillna(0)
df_resp_containment['Description'] = df_resp.apply(lambda x: "Stringency Index: {StringencyIndexForDisplay}<br>Confirmed Deaths: {ConfirmedDeaths}<br>School closing: {C1_School closing}<br>Workplace closing: {C2_Workplace closing}<br>Cancel public events: {C3_Cancel public events}<br>Restrictions on gatherings: {C4_Restrictions on gatherings}<br>Close public transport: {C5_Close public transport}<br>Stay at home requirements: {C6_Stay at home requirements}<br>Restrictions on internal movement: {C7_Restrictions on internal movement}<br>International travel controls: {C8_International travel controls}".format(**x), axis=1)

df_resp_containment


# ### Economic response dataframe

# In[88]:


#'E1_Income support', 'E1_Flag', 'E1_Notes', 'E2_Debt/contract relief', 'E2_Notes', 'E3_Fiscal measures', 'E3_Notes', 'E4_International support', 'E4_Notes'
# 'Country', 'Date', 'ConfirmedDeaths', 
df_resp_economic = df_resp[['E1_Income support', 'E1_Flag', 'E2_Debt/contract relief', 'E3_Fiscal measures']].copy().fillna(0)

df_resp_economic.loc[:,'E3_Fiscal measures'] = 3 * df_resp_economic['E3_Fiscal measures']/df_resp_economic['E3_Fiscal measures'].max()

df_resp_economic['Economic Index'] = df_resp_economic.sum(axis=1)

df_resp_economic = df_resp[['Country', 'Date', 'ConfirmedDeaths']].join(df_resp_economic[['Economic Index']])

def make_desc(x):
    return """Income support: {E1_Income support}<br>
    Debt/contract relief: {E2_Debt/contract relief}<br>
    Fiscal measures: {E3_Fiscal measures}<br>""".format(**x)

df_resp_economic['Description'] = df_resp.apply(lambda x: make_desc(x), axis=1)


df_resp_economic


# ### Health response dataframe

# In[89]:


# 'H1_Public information campaigns', 'H1_Flag', 'H1_Notes', 'H2_Testing policy', 'H2_Notes', 'H3_Contact tracing', 'H3_Notes', 'H4_Emergency investment in healthcare', 'H4_Notes', 'H5_Investment in vaccines', 'H5_Notes'
# 'Country', 'Date', 'ConfirmedDeaths', 
df_resp_health = df_resp[['H1_Public information campaigns', 'H1_Flag', 'H2_Testing policy','H3_Contact tracing', 'H4_Emergency investment in healthcare', 'H5_Investment in vaccines']].copy().fillna(0)

df_resp_health.loc[:,'H4_Emergency investment in healthcare'] = 2 * df_resp_health['H4_Emergency investment in healthcare']/df_resp_health['H4_Emergency investment in healthcare'].max()
df_resp_health.loc[:,'H5_Investment in vaccines'] = df_resp_health['H5_Investment in vaccines']/df_resp_health['H5_Investment in vaccines'].max()

df_resp_health['Health Index'] = df_resp_health.sum(axis=1)

df_resp_health = df_resp[['Country', 'Date', 'ConfirmedDeaths']].join(df_resp_health[['Health Index']])

def make_desc(x):
    return """Public information campaigns: {H1_Public information campaigns}<br>
    Testing policy: {H2_Testing policy}<br>
    Contact tracing: {H3_Contact tracing}<br>
    Emergency investment in healthcare: {H4_Emergency investment in healthcare}<br>
    Investment in vaccines: {H5_Investment in vaccines}
    """.format(**x)

df_resp_health['Description'] = df_resp.apply(lambda x: make_desc(x), axis=1)

df_resp_health


# ### Making response events dataframe

# In[90]:


df_resp_events = df_resp[['Country', 'Date'] + notes_cols]

df_resp_events = df_resp_events.melt(['Country', 'Date']).dropna(how='any')

df_resp_events = df_resp_events.groupby(['Country', 'variable', 'value'])['Date'].min().reset_index()

df_resp_events['type'] = 'Other'
df_resp_events.loc[df_resp_events.variable.str.startswith('C'),'type'] = 'Containment'
df_resp_events.loc[df_resp_events.variable.str.startswith('H'),'type'] = 'Health'
df_resp_events.loc[df_resp_events.variable.str.startswith('E'),'type'] = 'Economic'

def event_desc(event):
    code = event['variable'].split('_')[0]
    desc = var_names.loc[code, 'label'] + ': ' + event['value']
    return desc

df_resp_events['Description'] = df_resp_events.apply(lambda e: event_desc(e), axis=1)

df_resp_events.drop(['value', 'variable'], axis=1).sort_values(['Country', 'Date']).head(250)


# ## Adding backend (analysis) variables
# 

# In[91]:


# extract the day of the 10th death for each country

df_start = df_complete[df_complete['TotalDeaths'] >= 10].groupby(['Country']).head(1)[['Country', 'Day']]

df_start.columns = ['Country', 'Day10thDeath']

df_start = df_start.melt('Country')

df_start


# In[92]:


df_countries_stats_backend = pd.concat([df_countries_stats_complete.drop('change', axis=1), df_start]).sort_values(['Country', 'variable'])

df_countries_stats_backend


# ## Output data

# In[93]:


df_all_countries.rename(columns={'Country':'Name'}).to_csv(data_dir + 'all_countries.csv', index=False)

df_all_countries_compare.rename(columns={'Country':'Name'}).to_csv(data_dir + 'all_countries_compare.csv', index=False)

df_complete.rename(columns={'Country':'Name'}).to_csv(data_dir + 'total_cases_countries_normalized.csv', index=False)

df_countries_stats_complete.rename(columns={'Country':'Name'}).to_csv(data_dir + 'live_stats_countries.csv', index=False, float_format='%.2f')

df_countries_stats_backend.to_csv(data_dir + 'backend_stats_countries.csv', index=False)

df_events_sr.rename(columns={'Country':'Name'}).to_csv(data_dir + 'situation_reports_countries_highlight.csv', index=False)

df_resp.rename(columns={'Country':'Name'}).to_csv(data_dir + 'response/official_response_countries.csv', index=False)

df_resp_containment.rename(columns={'Country':'Name'}).to_csv(data_dir + 'response/official_response_containment_countries.csv', index=False)

df_resp_economic.rename(columns={'Country':'Name'}).to_csv(data_dir + 'response/official_response_economic_countries.csv', index=False)

df_resp_health.rename(columns={'Country':'Name'}).to_csv(data_dir + 'response/official_response_health_countries.csv', index=False)

df_resp_events.rename(columns={'Country':'Name'}).to_csv(data_dir + 'response/official_response_events_countries.csv', index=False)


# df_events_sr_quar.to_csv(data_dir + 'situation_reports_quarantine_filter.csv', index=False)


# In[ ]:




