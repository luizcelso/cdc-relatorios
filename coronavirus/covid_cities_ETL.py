#!/usr/bin/env python
# coding: utf-8

# # COVID-19 Data Processing - Cities
# 
# 
# COVID-19 cases data from [Our World in Data](www.ourworldindata.org)
# 

# In[ ]:





# In[19]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import seaborn as sns; sns.set()
from datetime import datetime
import calendar
import os
import io
import gzip
from urllib.request import Request, urlopen

pd.set_option('display.max_rows', 6000)
pd.set_option('display.max_columns', 100)
pd.set_option('display.width', 1000)


# ## Gather source data
# 
# Total cases and total deths by contry data from ourworldindata.org
# 

# In[20]:


data_dir = 'data/'

# df_orig = pd.read_csv(data_dir + 'total-cases-covid-19-who.csv')

url_cases = 'https://data.brasil.io/dataset/covid19/caso.csv.gz'

request = Request(url_cases, headers={"User-Agent": "python-urllib"})
response = urlopen(request)
data = response.read()

dfile = gzip.GzipFile(fileobj=io.BytesIO(data))

# sort values and fill missing values with last
df_source_cases = pd.read_csv(dfile).sort_values('date')#.ffill()
    
# df_orig.columns = ['Country', 'Code', 'Day', 'Total']

df_source_cases.head()


# In[21]:


df_orig = df_source_cases[(df_source_cases['place_type'] == 'city') & (df_source_cases['city'] != 'Importados/Indefinidos')]


df_orig['CityName'] = df_orig['city'] + '-' + df_orig['state']

df_orig = df_orig.rename(columns={'date':'Date', 'confirmed':'Total', 'deaths': 'TotalDeaths', 'CityName':'City', 'state':'State'})

df_stats = df_orig[df_orig['is_last']][['City', 'city_ibge_code', 'estimated_population_2019']]

df_stats.head()


# In[22]:


df_orig = df_orig[['City', 'Date', 'Total', 'TotalDeaths', 'city_ibge_code']]

df_orig.head()


# ## Data Cleaning
# 
# 

# In[23]:


from datetime import date
import datetime

# generating all valid dates
date_first = '2019-12-31'
date_first_obj = datetime.datetime.strptime(date_first, '%Y-%m-%d').date()

#today_obj = date.today()
today_obj = datetime.datetime.strptime(df_orig['Date'].max(), '%Y-%m-%d').date() 

delta = today_obj - date_first_obj

date_list = [str(date_first_obj + datetime.timedelta(days=x)) for x in range(0, delta.days + 1)]


date_df = pd.DataFrame({'Date':date_list})

date_df = date_df.reset_index().rename(columns={'index':'Day'})

date_df.head()


# In[24]:


# df_orig = df_orig.dropna()

# df_orig = df_orig[~df_orig['Country'].isin(['World', 'International'])]

df_orig = df_orig.sort_values(['City', 'Date'])

df_orig.head()


# In[25]:


# Fix missing values

df_cases_piv = df_orig.pivot(index='City', columns='Date', values='Total').reindex(date_df['Date'], axis = 1)                               .ffill(axis = 1).replace(0, np.nan).ffill(axis = 1)

df_cases = df_cases_piv.reset_index().melt(id_vars=['City']).rename(columns={'value':'Total'})

df_cases = df_cases.set_index(['City', 'Date'])

df_cases.loc[('Brasília-DF'),].tail()


# In[26]:


# Fix missing values

df_deaths_piv = df_orig.pivot(index='City', columns='Date', values='TotalDeaths').reindex(date_df['Date'], axis = 1)                       .ffill(axis = 1).replace(0, np.nan).ffill(axis = 1)

df_deaths = df_deaths_piv.reset_index().melt(id_vars=['City']).rename(columns={'value':'TotalDeaths'})

df_deaths = df_deaths.set_index(['City', 'Date'])

df_deaths.loc[('Curitiba-PR'),].tail()


# In[27]:


df_cleaned = df_cases.join(df_deaths).reset_index()

df_cleaned = df_cleaned.merge(df_orig[['City', 'city_ibge_code']].drop_duplicates(), how='left').merge(date_df, how='left')

df_orig = df_cleaned

df_orig.head()


# In[28]:


# generates DataFrame with all cities in the dataset

df_all_cities = pd.DataFrame({'City': df_orig['City'].unique()})

df_all_cities.head()


# In[29]:


# generates DataFrame with all cities with more than 10 deaths

df_all_cities_compare = pd.DataFrame({'City': df_orig.query('TotalDeaths > 10')['City'].unique()}).sort_values('City')

df_all_cities_compare.head(100)


# ## Adding per 100k stats
# 
# 

# In[30]:


df_pop = df_stats

df_pop = df_pop.drop(['city_ibge_code'], axis=1).rename(columns={'estimated_population_2019': 'Population'})

df_pop.head()


# In[31]:


df_orig = pd.merge(df_orig, df_pop)

df_orig.head()


# In[32]:


df_orig['TotalPer100k'] = df_orig['Total'] / df_orig['Population'] * 100000
df_orig['TotalDeathsPer100k'] = df_orig['TotalDeaths'] / df_orig['Population'] * 100000
df_orig = df_orig.drop('Population', axis = 1)

df_orig.head()


# In[33]:


df_orig = pd.merge(df_orig, df_pop)

df_orig.head()


# In[34]:


df_orig['TotalPer100k'] = df_orig['Total'] / df_orig['Population'] * 100000
df_orig['TotalDeathsPer100k'] = df_orig['TotalDeaths'] / df_orig['Population'] * 100000
df_orig = df_orig.drop('Population', axis = 1)

df_orig.head()


# ## Normalizing days to start after 50 cases
# 
# (not used currently)

# In[35]:


df_orig_start_day = df_orig[df_orig['Total'] >= 50]

df_orig_start_day.head()


# Start counting the days after first day with 50 total cases.

# In[36]:


df_orig_start_day = df_orig[df_orig['Total'] >= 50]

df_orig_start_day.head()


# In[37]:


df_orig_start_day['DayNorm'] =  df_orig_start_day[['Day']] - df_orig_start_day.groupby(['City'])[['Day']].transform('min') 

df_orig_start_day.sample(10)

df_orig_start_day = df_orig_start_day[['City', 'Day', 'DayNorm']]

df_orig_start_day.sort_values(['City', 'Day'])


# In[38]:


df_orig_norm = pd.merge(df_orig, df_orig_start_day, how='left').sort_values(['City', 'Day'])

df_orig_norm.set_index('City').loc[('Brasília-DF'),]


# ## Calculate daily growth rates

# In[39]:


df_growth = df_orig_norm.copy().sort_values(['City', 'Date'])

df_growth.head()


# In[40]:


df_growth_grouped = df_growth[['City', 'Day', 'Total', 'TotalDeaths']].set_index(['City', 'Day']) #.sort_index()


# In[41]:


# df_growth_grouped = df_growth.groupby(['City', 'Day']).max()[['Total', 'TotalDeaths']] #.sort_index()

df_growth_grouped.head(10)

# df_growth_grouped.loc['Brasília-DF',]


# In[42]:


df_growth_grouped['TotalPrev'] = df_growth_grouped.groupby(['City']).shift(1)['Total']

df_growth_grouped['GrowthRate'] = df_growth_grouped['Total']/df_growth_grouped['TotalPrev']

df_growth_grouped['TotalPrevDeaths'] = df_growth_grouped.groupby(['City']).shift(1)['TotalDeaths']

df_growth_grouped['GrowthRateDeaths'] = df_growth_grouped['TotalDeaths']/df_growth_grouped['TotalPrevDeaths']

df_growth_grouped = df_growth_grouped.replace([np.inf, -np.inf], np.nan)

df_growth_grouped.head()


# In[43]:


df_growth_rate = df_growth_grouped[['GrowthRate', 'GrowthRateDeaths']].dropna(subset=['GrowthRate'])

df_growth_rate.head()


# In[44]:


df_complete = pd.merge(df_orig_norm, df_growth_rate.reset_index(), how='left')

df_complete.head()


# In[45]:



df_complete['DaysToDouble'] = df_complete['GrowthRate'].apply(lambda x: math.log(2, x) if x > 1 else x)

df_complete['DaysToDoubleDeaths'] = df_complete['GrowthRateDeaths'].apply(lambda x: math.log(2, x) if x > 1 else x)

df_complete.head()


# ## Calculate weekly growth
# 
# 

# In[46]:


df_complete.set_index('City').loc[('Brasília-DF'),]


# In[47]:


df_complete[['WeeklyGrowth', 'WeeklyGrowthDeaths']] = (df_complete.groupby(['City'])[['Total', 'TotalDeaths']].pct_change(periods=7) * 100).replace([np.inf, -np.inf], np.nan)

df_complete.head()


# ## Generate last day stats by City
# 

# In[48]:


df_complete.set_index('City').loc['Brasília-DF']


# In[49]:


# last two days
dates = df_complete['Date'].sort_values().unique()[-2:]

columns_stats = ['Total', 'TotalPer100k', 'GrowthRate', 'DaysToDouble', 'WeeklyGrowth', 'TotalDeaths', 'TotalDeathsPer100k', 'GrowthRateDeaths', 'DaysToDoubleDeaths', 'WeeklyGrowthDeaths']

df_cities_before = df_complete.groupby(['City']).tail(2).groupby(['City']).head(1).set_index('City')[columns_stats]

df_cities_now = df_complete.groupby(['City']).tail(1).set_index('City')[columns_stats]

df_cities_stats_change = ((df_cities_now/df_cities_before) - 1) * 100

df_cities_stats_change = df_cities_stats_change.reset_index().melt('City').rename(columns={'value':'change'})

df_cities_stats_complete = pd.merge(df_cities_now.reset_index().melt('City'), df_cities_stats_change, how='outer')

df_cities_stats_complete.head(10)


# In[50]:


df_cities_stats_complete.query("City == 'Vitória-ES'")


# ## Situation reports
# 
# 

# In[51]:




# df_events_sr = pd.read_csv(data_dir + 'john-hopkins-processed.csv')

# df_events_sr['Source'] = 'Johns Hopkins Situation Reports'

# df_events_sr['Country'] = df_events_sr['Country'].str.replace('US', 'United States')

# df_events_sr


# In[52]:


# df_keywords = pd.read_csv(data_dir + 'keywords_JH_reports.csv')

# df_keywords_grouped = df_keywords.groupby(['highlight'])['keyword'].apply(lambda x: "|".join(x))

# for h, k in df_keywords_grouped.iteritems():
#     df_events_sr.loc[df_events_sr['FullText'].str.contains(k), 'Highlight'] = h

# df_events_sr['Highlight'] = df_events_sr['Highlight'].fillna(0)

# df_events_sr


# ## Output data

# In[53]:


df_all_cities.rename(columns={'City':'Name'}).to_csv(data_dir + 'all_cities.csv', index=False)

df_all_cities_compare.rename(columns={'City':'Name'}).to_csv(data_dir + 'all_cities_compare.csv', index=False)

df_complete.rename(columns={'City':'Name'}).to_csv(data_dir + 'total_cases_cities_normalized.csv', index=False)

df_cities_stats_complete.rename(columns={'City':'Name'}).to_csv(data_dir + 'live_stats_cities.csv', index=False, float_format='%.2f')

df_stats.rename(columns={'City':'Name'}).to_csv('../data/municipios/' + 'stats_population.csv', index=False)


# In[ ]:




