Mantive os nomes originais dos arquivos para ser f�cil baixar de novo se for preciso.
Fiz essa tabela "original file names + description"
para saberem o que cada planilha tem sem precisar abrir.

Deixei os arquivos .xls originais numa pasta � parte
para o caso dos .csv que fiz a partir deles darem algum problema.

Aten��o �s unidades dos dados.
Alguns est�o em unidades, outros milhares, outros porcentagens.

Quanto aos nomes dos pa�ses, vai ser necess�rio padronizar,
porque existem pa�ses que t�m nomes diferentes dependendo da fonte dos dados
(para dar um exemplo extremo, Eswatini � o mesmo que Swaziland).
O jeito que eu sei para corrigir isso � manualmente.
Mas se ajudar, eu posso fazer uma tabela de correspond�ncias
e depois quem souber fazer isso pode us�-la para agilizar a corre��o em todas as planilhas.
Celso, se vc quiser que eu prepare essa tabela auxiliar me manda a nomenclatura
que vc tem utilizado para eu fazer a correspond�ncia.

Ainda n�o baixei os dados de temperatura e humidade, mas farei isso em breve.

Qualquer d�vida, o meu email � zulmira.gamito@gmail.com