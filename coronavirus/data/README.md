# Data documentation

The text below describes the main data files used in the project. 

## coronavirus_data_live_stats_countries.csv

**Description**: Current epidemic statistics for the world's countries.

**Variables**: 'Total' (cases), 'TotalPer100k', 'GrowthRate' (increase factor from previous day), 'DaysToDouble' (number of days to double cases based on current rate), 'WeeklyGrowth' (percentage of growth in the previous 7 days), 'TotalDeaths', 'TotalDeathsPer100k', 'GrowthRateDeaths', 'DaysToDoubleDeaths', 'WeeklyGrowthDeaths'

**Data sources**: 

* [Our World in Data](https://ourworldindata.org/coronavirus): epidemic data by country
* [World Bank](https://www.worldbank.org/): Population data

## coronavirus_data_total_cases_countries_normalized.csv

**Description**: Epidemic data for the world's countries over time.

**Variables**: 'Country Name', 'Date', 'Total', 'Day' (days since beginning of the pandemic), 'TotalDeaths', 'TotalPer100k', 'TotalDeathsPer100k', 'DayNorm' (days since country's 10th death, 'GrowthRate' (increase factor from previous day), 'GrowthRateDeaths', 'DaysToDouble' (number of days to double cases based on current rate), 'DaysToDoubleDeaths', 'WeeklyGrowth' (percentage of growth in the previous 7 days), 'WeeklyGrowthDeaths'

**Data sources**: 

* [Our World in Data](https://ourworldindata.org/coronavirus): epidemic data by country
* [World Bank](https://www.worldbank.org/): Population data

## coronavirus_data_live_stats_cities.csv

**Description**: Current epidemic statistics for Brazilian cities.

**Variables**: 'Total' (cases), 'TotalPer100k', 'GrowthRate' (increase factor from previous day), 'DaysToDouble' (number of days to double cases based on current rate), 'WeeklyGrowth' (percentage of growth in the previous 7 days), 'TotalDeaths', 'TotalDeathsPer100k', 'GrowthRateDeaths', 'DaysToDoubleDeaths', 'WeeklyGrowthDeaths'

**Data sources**: 

* [Brasil.io](https://brasil.io/): Epidemic and population data

## coronavirus_data_total_cases_cities_normalized.csv

**Description**: Epidemic data for Brazilian cities over time.

**Variables**: 'City Name', 'Date', 'Total', 'Day' (days since beginning of the pandemic), 'TotalDeaths', 'TotalPer100k', 'TotalDeathsPer100k', 'DayNorm' (days since country's 10th death, 'GrowthRate' (increase factor from previous day), 'GrowthRateDeaths', 'DaysToDouble' (number of days to double cases based on current rate), 'DaysToDoubleDeaths', 'WeeklyGrowth' (percentage of growth in the previous 7 days), 'WeeklyGrowthDeaths'

* [Brasil.io](https://brasil.io/): Epidemic and population data

## coronavirus_data_socio_stats_cities.csv

**Description**: Current general (socioeconomic and environmental) statistics for Brazilian cities.

**Variables**: 'PIB per Capita', 'Quantidade de Leitos - Pneumologia', 'Quantidade de Leitos - Complementares', 'Quantidade de Leitos - Internacao', 'Quantidade de Profissionais de Saude', 'Quantidade de Ventiladores e Respiradores', 'Ventiladores e Respiradores em uso', 'Área (km²)', 'Populacao (2019)', 'Densidade Demográfica (hab/km²)', 'Quantidade de Leitos - Pneumologia (por 100 mil hab)', 'Existem Favelas (s/n)'

**Data sources**: 

* [Brasil.io](https://brasil.io/): Socioeconomic data

## coronavirus_data_socio_stats_countries.csv

**Description**: Current general (socioeconomic and environmental) statistics for the world's countries.

**Variables**: 'Population', 'Population density (people per sq. km of land area)', 'Urban population (% of total population)', 'Population in urban agglomerations of more than 1 million (% of total population)', 'Population living in slums (% of urban population)', 'Population ages 0-14 (% of total population)', 'Population ages 15-64 (% of total population)', 'Population ages 65 and above (% of total population)', 'Life expectancy at birth, total (years)', 'Cause of death, by non-communicable diseases (% of total)', 'Cause of death, by communicable diseases and maternal, prenatal and nutrition conditions (% of total)', 'Prevalence of undernourishment (% of population)', 'GDP per capita (current US$)', 'Current health expenditure (% of GDP)', 'Current health expenditure per capita (current US$)', 'International tourism, number of arrivals', 'Average Temperature April'

**Data sources**: 

* [World Bank](https://www.worldbank.org/): Socioeconomic data
* [Berkeley Earth - Kaggle](https://www.kaggle.com/berkeleyearth/climate-change-earth-surface-temperature-data/version/2#GlobalLandTemperaturesByCountry.csv): Temperature data.