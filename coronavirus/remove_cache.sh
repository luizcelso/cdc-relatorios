#!/bin/sh

cd /home/guilmour/cdc-relatorios/coronavirus/

#git pull
echo "Matando processos Flask..."
pkill -f flask

echo "Removendo Cache..."
sudo rm -r /home/guilmour/nginx-cache2/

echo "Iniciando novo nohup pipenv..."
export LC_ALL=C.UTF-8
pipenv install

nohup pipenv run flask run &

echo "Reiniciando NGINX..."
sudo systemctl restart nginx.service
