from flask import render_template, flash, redirect, send_file, url_for, abort, request, jsonify
from app import app
from datetime import datetime, timedelta
from app.models import Event
from app.forms import CompareCountryForm, CompareCityForm, ResponseCountryForm, SubmitEventRequestForm
from app.compare import get_similar_places, get_fig_compare_rates, get_fig_compare_doubling_rates, \
    get_all_places, get_timeline_list, get_place_live_stats, get_place_socio_stats, get_similar_places_socio, \
    get_fig_response, get_places_by_variable, get_html_compare_response, get_html_compare_response_econ
from app.news import get_posts, get_post_by_id



@app.context_processor
def inject_countries():
    return dict(countries=get_all_places(level = "countries"))
@app.context_processor
def inject_cities():
    return dict(cities=get_all_places(level = "cities"))

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    posts_blog = get_posts()
    type = 'list_posts'
    form_countries = CompareCountryForm()
    form_cities = CompareCityForm()
    return render_template('index.html', form_countries=form_countries, form_cities=form_cities,  posts_blog=posts_blog, type = type)

@app.route('/countries', methods=['GET', 'POST'])
def countries():
    form_countries = CompareCountryForm()
    if form_countries.validate_on_submit():
        return redirect(url_for("country", country=form_countries.countries.data))
    return redirect(url_for("index"))


@app.route('/cities', methods=['GET', 'POST'])
def cities():
    form_cities = CompareCityForm()
    if form_cities.validate_on_submit():
        return redirect(url_for("city", city=form_cities.cities.data))
    return redirect(url_for("index"))


@app.route('/country/<country>')
def country(country):
    simcountries = get_similar_places(place = country, level = 'countries')
    simcountries_socio = get_similar_places_socio(place = country, level = 'countries')

    live_stats = get_place_live_stats(place = country, level = 'countries')
    return render_template('compare_init.html', country=country, level = 'countries', simcountries=simcountries, simcountries_socio = simcountries_socio, live_stats=live_stats)

@app.route('/place/<place>/<level>')
def place(place, level):
    if level == 'countries':
        return redirect(url_for("country", country = place))
    else:
        return redirect(url_for("city", city = place))



@app.route('/city/<city>')
def city(city):
    simcities = get_similar_places(place = city, level = 'cities')
    simcities_socio = get_similar_places_socio(place = city, level = 'cities')
    live_stats = get_place_live_stats(place = city, level = 'cities')
    return render_template('compare_init_city.html', city=city, level='cities', simplaces=simcities, simplaces_socio = simcities_socio, live_stats=live_stats)


@app.route('/comparison/<place>/<place2>/<level>')
def comparison(place, place2, level):
    html_compare_resp = get_html_compare_response(place, place2, level) if level == 'countries' else ''
    # html_compare_resp_econ = get_html_compare_response_econ(place, place2, level) if level == 'countries' else ''
    html_compare_resp_econ = None
    return render_template("compare_places.html", place=place, place2 = place2, level = level,
                           ls_place = get_place_live_stats(place = place, level = level), ls_place2 = get_place_live_stats(place = place2, level = level),
                            ss_place = get_place_socio_stats(place = place, level = level), ss_place2 = get_place_socio_stats(place = place2, level = level),
                           html_compare_resp = html_compare_resp, html_compare_resp_econ = html_compare_resp_econ
                           )

@app.route('/figcomparerates/<place>/<place2>/<level>.png')
@app.route('/figcomparerates/<place>/<place2>/<level>/<scale>/<y>/<mode>/<priority>.png')
def figcomparerates(place, place2, level, scale='log', y='total', mode='static', priority = 'now'):
    img = get_fig_compare_rates(place, place2, level, scale=scale, y=y, mode=mode, priority = priority)
    return send_file(img, mimetype='image/png')

@app.route('/figcomparedblrates/<place>/<place2>/<level>.png')
def figcomparedblrates(place, place2, level):
    img = get_fig_compare_doubling_rates(place, place2, level)
    return send_file(img, mimetype='image/png')

@app.route('/htmlcompareresp/<place>/<place2>/<level>.html')
@app.route('/htmlcompareresp/<place>/<place2>/<level>/<scale>/<y>/<mode>/<priority>.html')
def htmlcompareresp(place, place2, level, scale='log', y='total', mode='static', priority = 'now'):
    img = get_html_compare_response(place, place2, level, scale=scale, y=y, mode=mode, priority = priority)
    return img

@app.route('/htmlcomparerespecon/<place>/<place2>/<level>.html')
@app.route('/htmlcomparerespecon/<place>/<place2>/<level>/<scale>/<y>/<mode>/<priority>.html')
def htmlcomparerespecon(place, place2, level, scale='log', y='total', mode='static', priority = 'now'):
    img = get_html_compare_response_econ(place, place2, level, scale=scale, y=y, mode=mode, priority = priority)
    return img


@app.route('/timeline/<place>/<place2>/<level>')
def timeline(place, place2, level):
    tl = get_timeline_list(place, place2, level = level)
    return render_template("timeline.html", place=place, place2 = place2, level = level, timeline = tl)

@app.route('/response')
def response():
    form = ResponseCountryForm()
    return render_template("response.html", form = form)

@app.route('/figresponse/<place>.png')
def figresponse(place):
    img = get_fig_response(place)
    return send_file(img, mimetype='image/png')

@app.route('/explore_countries')
def explore_countries():
    by_pop_65 = get_places_by_variable(type = 'socio', level = 'countries', variable = 'Population ages 65 and above (% of total population)', ascending = False)
    by_TotalDeathsPer100k = get_places_by_variable(type = 'live', level = 'countries', variable = 'TotalDeathsPer100k', ascending = False)
    by_WeeklyGrowthDeaths = get_places_by_variable(type = 'live', level = 'countries', variable = 'WeeklyGrowthDeaths', ascending = False)
    return render_template('explore_countries.html', by_pop_65=by_pop_65, by_TotalDeathsPer100k=by_TotalDeathsPer100k, by_WeeklyGrowthDeaths=by_WeeklyGrowthDeaths, level='countries')

@app.route('/explore_cities')
def explore_cities():
    by_leitos_pc = get_places_by_variable(type = 'socio', level = 'cities', variable = 'Quantidade de Leitos - Pneumologia (por 100 mil hab)', ascending = True)
    by_TotalDeathsPer100k = get_places_by_variable(type = 'live', level = 'cities', variable = 'TotalDeathsPer100k', ascending = False)
    by_WeeklyGrowthDeaths = get_places_by_variable(type = 'live', level = 'cities', variable = 'WeeklyGrowthDeaths', ascending = False)
    return render_template('explore_cities.html', by_leitos_pc=by_leitos_pc,by_TotalDeathsPer100k=by_TotalDeathsPer100k, by_WeeklyGrowthDeaths=by_WeeklyGrowthDeaths, level='countries')


@app.route('/about-us')
def aboutUs():
    return render_template("about_us.html")

@app.route('/posts/')
def news():
    posts_blog = get_posts()
    type = 'list_posts'
    return render_template("news.html", posts_blog=posts_blog, type = type)

@app.route('/posts/<slug>')
def newsId(slug):
    post = get_post_by_id(slug)[0]
    authorname = get_post_by_id(slug)[1]
    type = 'single_post'
    return render_template("news.html", post=post, type = type, authorname=authorname)

@app.route('/faq')
def faq():
    return render_template("faq.html")

@app.route('/report-event', methods=['GET', 'POST'])
def reportEvent():
    form_event = SubmitEventRequestForm()
    if form_event.validate_on_submit():
        event = Event()
        form_event.populate_obj(event)
        event.save()
        return render_template('submit_event_success.html')

    return render_template('submit_event.html', form_event=form_event)

@app.route('/new-event', methods=['POST'])
def newEvent():
    payload = request.json
    required_fields = ['place', 'type', 'source']
    # Check the required fields
    if not (payload and all(field in payload and payload[field] != '' for field in required_fields)):
        abort(400)

    # Check if description for type other
    if payload['type'] == 'other' and payload['description'] == '':
        abort(400)

    # Create event
    new_event = Event(place=payload['place'],
                        date=datetime.strptime(payload['date'],'%Y-%m-%d'),
                        desc=payload['type'],
                        fulltext=payload['description'],
                        source=payload['source'])

    new_event.save()
    return new_event.json()
