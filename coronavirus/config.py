import os

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    SEND_FILE_MAX_AGE_DEFAULT = 0
    BABEL_TRANSLATION_DIRECTORIES = '/home/guilmour/gits/cdc-relatorios/coronavirus/app/translations/'
    print(BABEL_TRANSLATION_DIRECTORIES)
    LANGUAGES = ['es', 'pt_BR', 'fr', 'en']
    # SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:root@127.0.0.1/coronavirus"
    SQLALCHEMY_DATABASE_URI = "sqlite:///{}".format(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'app/coronavirus.db'))
    SQLALCHEMY_TRACK_MODIFICATIONS = False
